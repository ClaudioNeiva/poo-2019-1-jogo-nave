package br.ucsal.bes.poo20191.game1;

import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Nave {

	private Image imagem;
	private Integer x;
	private Integer y;

	public Nave(Integer x, Integer y) throws IOException {
		this.x = x;
		this.y = y;
		imagem = ImageIO.read(new File("resource/imagens/nave.png"));
	}

	public void draw(Graphics g) {
		g.drawImage(imagem, x, y, null);
	}

	public void moverEsquerda() {
		x--;
	}

}
