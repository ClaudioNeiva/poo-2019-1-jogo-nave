package br.ucsal.bes.poo20191.game1;

import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class PainelJogo extends JPanel implements Runnable {

	private static final Integer X_NAVE_DEFAULT = 300;
	private static final Integer Y_NAVE_DEFAULT = 200;

	private Image imagemFundo;

	private Nave nave;

	public PainelJogo() throws IOException {
		setDoubleBuffered(true);
		imagemFundo = ImageIO.read(new File("resource/imagens/fundo.jpg"));
		nave = new Nave(X_NAVE_DEFAULT, Y_NAVE_DEFAULT);
		inicializarThreadJogo();
	}

	private void inicializarThreadJogo() {
		Thread thread = new Thread(this);
		thread.start();
	}

	@Override
	public void run() {
		while (true) {
			update();
			repaint();
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	private void update() {
		nave.moverEsquerda();
	}

	@Override
	public void paint(Graphics g) {
		g.drawImage(imagemFundo, 0, 0, null);
		nave.draw(g);
	}

}
