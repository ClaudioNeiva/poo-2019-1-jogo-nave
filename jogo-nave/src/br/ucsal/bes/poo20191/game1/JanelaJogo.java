package br.ucsal.bes.poo20191.game1;

import java.io.IOException;

import javax.swing.JFrame;

public class JanelaJogo extends JFrame {

	private static final int LARGURA = 500;
	private static final int ALTURA = 400;
	
	private PainelJogo painelJogo;

	public JanelaJogo() throws IOException {
		inicializarJanela();
	}

	private void inicializarJanela() throws IOException {
		setTitle("Game de POO 2019-1");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setSize(LARGURA, ALTURA);
		setLocationRelativeTo(null);
		setResizable(true);
		
		painelJogo = new PainelJogo();
		add(painelJogo);
		
		setVisible(true);
	}

	public static void main(String[] args) throws IOException {
		new JanelaJogo();
	}

}
