# POO 2019-1 Jogo da Nave

gameLoop <br/>
	atualizar <br/>
	renderizar <br/>
	pausar <br/>
 <br/>
Interfaces <br/>
	KeyListener  <br/>
		keyTyped <br/>
		keyPressed <br/>
		keyReleased <br/>
		addKeyListener(this); <br/>
	Runnable <br/>
		run <br/>
 <br/>
Classes <br/>
	JFrame  <br/>
		setTitle(""); <br/>
		setDefaultCloseOperation(EXIT_ON_CLOSE); <br/>
		setSize(w, h); <br/>
		setLocationRelativeTo(null); <br/>
		setResizable(false); <br/>
	JPanel <br/>
		setDoubleBuffered(true); <br/>
		g.drawImage(fundo, x, y, null); <br/>
		paint <br/>
	ImageIO	 <br/>
		read(new File("resource/..."); <br/>
	Thread <br/>
		sleep <br/>
	
